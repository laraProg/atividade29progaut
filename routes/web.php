<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Biblioteca;

Route::get('/',function(){
    return view ('welcome');
});

/*Route::get('/biblioteca',function(){
    return view ('pagbiblioteca');
});*/

Route::get('/biblioteca', [Biblioteca::class, 'retorno']);